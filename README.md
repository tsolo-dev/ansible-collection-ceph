# Ansible Collection - tsolo.ceph

This collection contains miscellaneous Ansible roles, plays, etc. used for Ceph installation and management.

Only works with Ansible 2.10 or newer.
