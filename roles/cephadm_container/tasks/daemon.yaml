# roles/cephadm_container/tasks/daemon.yaml
---
# Create a service that permanently runs Cephadm container.
# This container is accessible on SSH.

- name: Remove container-tsolo-cephadm service
  ansible.builtin.systemd:
    name: container-tsolo-cephadm.service
    state: stopped
    enabled: false
  failed_when: false

- name: Check that we only have one cephadm_container defined
  ansible.builtin.fail:
    msg: "There should only be one cephadm_container node. Nodes: {{ groups.get('cephadm_container', []) }}"
  when: (groups.get('cephadm_container', []) | length) != 1
  any_errors_fatal: true

- name: Prepare variables
  ansible.builtin.set_fact:
    _cephadm_container_user: "{{ _container_hostvars.ansible_ssh_user }}"
    _cephadm_container_password: "{{ _container_hostvars.ansible_ssh_pass }}"
    _cephadm_container_port: "{{ _container_hostvars.ansible_port | int }}"
  vars:
    _container_hostvars: "{{ hostvars[groups['cephadm_container'][0]] }}"

- name: Create service directory
  ansible.builtin.file:
    path: /srv/cephadm
    state: directory
    mode: "0770"

- name: Find ssh_host files
  ansible.builtin.find:
    path: /etc/ssh
    patterns: ssh_host_*
    recurse: false
    file_type: file
  register: ssh_host_keys

- name: Copy SSH host keys to container directory
  ansible.builtin.copy:
    src: "{{ item.path }}"
    dest: /srv/cephadm/
    mode: "0600"
    owner: root
    remote_src: true
  loop: "{{ ssh_host_keys.files }}"

- name: Create the Containerfile from template
  ansible.builtin.template:
    src: cephadm_daemon_Containerfile.j2
    dest: /srv/cephadm/Containerfile
    mode: "0644"
  register: containerfile

- name: Build the container image
  containers.podman.podman_image:
    name: tsolo-cephadm:v{{ ceph_version }}
    path: /srv/cephadm
    force: "{{ containerfile.changed }}"
    build:
      extra_args: --build-arg CONTAINER_USER={{ _cephadm_container_user }} --build-arg CONTAINER_PASS={{ _cephadm_container_password }}

- name: Get fsid of the cluster
  tsolo.ceph.ceph_cmd:
    argv:
      - fsid
  register: _ceph_cmd_fsid
  changed_when: false

- name: Open ports in firewall
  when: firewalld_enabled
  ansible.posix.firewalld:
    port: "{{ item }}/tcp"
    permanent: true
    state: enabled
    zone: public
    immediate: true
  loop:
    - "{{ _cephadm_container_port }}"

- name: Deploy Cephadm container as a service
  tsolo.node.srv_podman:
    name: cephadm
    container:
      hostname: "{{ groups['cephadm_container'][0] }}"
      image: tsolo-cephadm
      label: v{{ ceph_version }}
      publish:
        - "{{ _cephadm_container_port }}:22"
      command: /usr/sbin/sshd -D -e
      volumes:
        - /var/log/ceph/{{ ceph_fsid }}:/var/log/ceph:z
        - /var/lib/ceph/{{ ceph_fsid }}/crash:/var/lib/ceph/crash:z
        - /run/systemd/journal:/run/systemd/journal
        - /dev:/dev
        - /run/udev:/run/udev
        - /sys:/sys
        - /run/lvm:/run/lvm
        - /run/lock/lvm:/run/lock/lvm
        - /:/rootfs
        - /var/lib/ceph/{{ ceph_fsid }}/config/:/etc/ceph/:ro
      environment:
        - NODE_NAME={{ inventory_hostname }}
    # ipc: host  # Not sure this is needed?
    # privileged: true  # Not sure this is needed?
    # group_add: # Not sure this is needed?
    #   - disk
    makefile:
      shell:
        commands:
          - /usr/bin/podman exec -it srv-cephadm bash
      ceph:
        commands:
          - /usr/bin/podman exec -it srv-cephadm ceph

- name: Start srv-cephadm service
  ansible.builtin.systemd:
    name: srv-cephadm.service
    state: started
    enabled: true
