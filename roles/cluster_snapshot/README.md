# tsolo.ceph/cluster_snapshot

Collect information that can be used to create a snapshot view of the cluster.

## Table of content

- [Default Variables](#default-variables)
  - [ceph_cluster_snapshot_health_only](#ceph_cluster_snapshot_health_only)
  - [ceph_cluster_snapshot_location](#ceph_cluster_snapshot_location)
  - [ceph_cluster_snapshot_timestamp](#ceph_cluster_snapshot_timestamp)
- [Dependencies](#dependencies)
- [License](#license)
- [Author](#author)

---

## Default Variables

### ceph_cluster_snapshot_health_only

#### Default value

```YAML
ceph_cluster_snapshot_health_only: false
```

### ceph_cluster_snapshot_location

The base directory where the cluster_snapshots are stored. Each snapshot event is stored in a timestamped directory.

#### Default value

```YAML
ceph_cluster_snapshot_location: /var/lib/ceph/cluster_snapshot
```

### ceph_cluster_snapshot_timestamp

#### Default value

```YAML
ceph_cluster_snapshot_timestamp: '{{ ansible_date_time.iso8601 }}'
```



## Dependencies

None.

## License

Apache-2.0

## Author

Tsolo.io
