# tsolo.ceph/configure_cluster

Configure a Ceph cluster

## Table of content

- [Default Variables](#default-variables)
  - [ceph_crush_rules](#ceph_crush_rules)
  - [ceph_dashboard_admin_password](#ceph_dashboard_admin_password)
  - [ceph_mgr_config](#ceph_mgr_config)
  - [ceph_mon_config](#ceph_mon_config)
  - [ceph_osd_config](#ceph_osd_config)
  - [ceph_telemetry_enabled](#ceph_telemetry_enabled)
- [Dependencies](#dependencies)
- [License](#license)
- [Author](#author)

---

## Default Variables

### ceph_crush_rules

A list of crush rules to create. Each crush rule can have the following fields.

- failure_domain - default to 'host'. - root - default to 'default'. - class - the device class, default to 'all'. - ec_k - K value for erasure coded pool. If this exists it crush rule is erasure and not replicated, default to 0. - ec_m - M value for erasure coded pool. default to 2.

#### Default value

```YAML
ceph_crush_rules: []
```

#### Example usage

```YAML
ceph_crush_rules:
  - failure_domain: osd
  - class: hdd
  - class: ssd
  - class: nvme
  - ec_k: 2
    ec_m: 2
```

### ceph_dashboard_admin_password

The initial password for the dashboard admin user.

#### Default value

```YAML
ceph_dashboard_admin_password: Password*12345
```

### ceph_mgr_config

Configuration values at the base of the configuration hierarchy. It is best to use the service specific config options like ceph_rgw_config. All the key-value pairs in ceph_mgr_config will be applied with the command `ceph config set global <key> <value>`. Use string for value to avoid parsing errors.

#### Default value

```YAML
ceph_mgr_config: {}
```

#### Example usage

```YAML
ceph_mgr_config:
  mgr_val: 9.2
```

### ceph_mon_config

Configuration values at the base of the configuration hierarchy. It is best to use the service specific config options like ceph_rgw_config. All the key-value pairs in ceph_mon_config will be applied with the command `ceph config set global <key> <value>`. Use string for value to avoid parsing errors.

#### Default value

```YAML
ceph_mon_config: {}
```

#### Example usage

```YAML
ceph_mon_config:
  mon_val: 200
```

### ceph_osd_config

Configuration values at the base of the configuration hierarchy. It is best to use the service specific config options like ceph_rgw_config. All the key-value pairs in ceph_osd_config will be applied with the command `ceph config set global <key> <value>`. Use string for value to avoid parsing errors.

#### Default value

```YAML
ceph_osd_config: {}
```

#### Example usage

```YAML
ceph_osd_config:
  osd_val: "true"
```

### ceph_telemetry_enabled

When enabled telemetry information about the cluster will be sent to Ceph foundation.

#### Default value

```YAML
ceph_telemetry_enabled: true
```



## Dependencies

None.

## License

Apache-2.0

## Author

Tsolo.io
