import datetime
import json
from pathlib import Path
from tempfile import mkstemp
from typing import List

from ansible.module_utils._text import to_native, to_text

# Shared utilities used in modules.


def prepare_ansible_result(extras: list):
    result = {
        "changed": False,
        "stdout": "",
        "stderr": "",
        "rc": None,
        "cmd": None,
        "start": datetime.datetime.now(),
        "end": None,
        "delta": None,
        "msg": "",
    }
    for _res in extras:
        result[_res] = None
    return result


class TsoloCommandBase:
    """The base class for the command utility classes.

    Attributes
    ----------
    result:
        A results dictionary used by Ansible module exit and fail methods.
    """

    def __init__(self, module, result: list = [], json_format=False):
        """Initialise TsoloRgwadminCommand.

        Parameters
        ----------
        module:
            The Ansible module that this is called from.
        result:
            A list of additional fields to add to the result.
        json_format:
            If the output of the command should be interpreted as JSON.
            The argument '--format=json' is added to the command.
        """
        self._module = module
        self._json_format = json_format
        self._cephadm_shell = module.params["cephadm_shell"]
        if self._json_format:
            result.append("json")
        self.result = prepare_ansible_result(result)

    def _finalise(self, msg=None, changed=None):
        """Update the result at the end of the module call.

        _finalise should be called just before failed or exit.
        """

        if msg:
            self.result["msg"] = msg
        if changed is False or changed is True:
            self.result["changed"] = changed

        self.result["msg"] = to_text(self.result["msg"])
        self.result["end"] = datetime.datetime.now()
        self.result["delta"] = to_text(self.result["end"] - self.result["start"])
        self.result["end"] = to_text(self.result["end"])
        self.result["start"] = to_text(self.result["start"])

    def _run_cmd(self, cmd: list[str]):
        if self._cephadm_shell:
            cmd = ["cephadm", "shell", "--", *cmd]
        if self._json_format:
            cmd.append("--format=json")

        _args = [to_native(arg, errors="surrogate_or_strict", nonstring="simplerepr") for arg in cmd]

        self.result["cmd"] = _args
        # Get the value
        _rc, _stdout, _stderr = self._module.run_command(_args, encoding=None)
        self.result["rc"] = _rc
        self.result["stdout"] = to_text(_stdout).strip()
        self.result["stderr"] = to_text(_stderr).strip()
        if _rc != 0:
            msg = f"Command '{_args}' exited with {_rc}"
            raise RuntimeError(msg)

        if self._json_format and self.result["stdout"]:
            self.result["json"] = json.loads(_stdout)
        return self.result["stdout"]

    def result_set(self, key, value):
        """Set a value in the result."""
        self.result[key] = value

    def fail(self, msg, changed=None):
        """Exit the module as a failed task.

        Optionally set the msg and changed fields in the result.
        """
        self._finalise(msg=msg, changed=changed)
        self._module.fail_json(**self.result)

    def exit(self, msg=None, changed=None):
        """Successfully exit the module.

        Optionally set the msg and changed fields in the result.
        """
        self._finalise(msg=msg, changed=changed)
        self._module.exit_json(**self.result)


class TsoloCephCommand(TsoloCommandBase):
    """A utility class to call the `ceph` command.

    Attributes
    ----------
    result:
        A results dictionary used by Ansible module exit and fail methods.
    """

    def __init__(
        self,
        module,
        result: list = [],
        json_format: bool = False,
        input_file_content: str = None,
    ):
        """Initialise TsoloCephCommand.

        Parameters
        ----------
        module:
            The Ansible module that this is called from.
        result:
            A list of additional fields to add to the result.
        json_format:
            If the output of the command should be interpreted as JSON.
            The argument '--format=json' is added to the command.
        input_file_content:
            Content of the file to use with the --in-file argument in ceph.
        """
        self._input_file_content = input_file_content
        # TsoloCommandBase.__init__(self, module, result, json_format=json_format)
        super().__init__(module, result, json_format=json_format)

    def _temp_input_file(self, content: str):
        filename = Path(mkstemp()[1])
        filename.write_text(content)
        if self._cephadm_shell:
            filename = Path("/rootfs") / filename
        return filename

    def cmd(self, args: list[str]):
        """Run the `ceph` command with the given arguments (args)."""

        _args = ["ceph", *args]

        if self._input_file_content:
            filename = self._temp_input_file(self._input_file_content)
            _args.append(f"--in-file={filename}")

        return self._run_cmd(_args)


class TsoloRgwadminCommand(TsoloCommandBase):
    """A utility class to call the `radosgw-admin` command.

    Attributes
    ----------
    result:
        A results dictionary used by Ansible module exit and fail methods.
    """

    def cmd(self, args: list[str]):
        """Run the `radosgw-admin` command with the given arguments (args)."""

        _args = ["radosgw-admin", *args]
        return self._run_cmd(_args)


class TsoloRbdCommand(TsoloCommandBase):
    """A utility class to call the `rbd` command.

    Attributes
    ----------
    result:
        A results dictionary used by Ansible module exit and fail methods.
    """

    def cmd(self, args: list[str]):
        """Run the `rbd` command with the given arguments (args)."""

        _args = ["rbd", *args]
        return self._run_cmd(_args)
