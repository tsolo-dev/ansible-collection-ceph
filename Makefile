.PHONY: docs
.DEFAULT_GOAL := help

help:
	@echo "Help"
	@grep -h -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

build: ## Package this Ansible collection
	ansible-galaxy collection build --force

install: build ## Local install this collection
	ansible-galaxy collection install $(shell ls -1 tsolo-*.tar.gz | sort | tail -n 1)  --force

check: ## Check the code for inconsistencies and errors
	hatch run lint:ansible-lint

format: ## Format the code
	hatch run lint:format-yaml .
	hatch run lint:ansible-lint --fix

spelling:
	hatch run lint:spell

docs: ## Create the documentation
	test -d ~/src/ansible-utilities/bin && ~/src/ansible-utilities/bin/update_collection
	hatch run docs:build
	hatch run lint:ansible-lint --fix # docs modifies file, it should not.

docs-serve: ## Serve the documentation as a web site
	hatch run docs:serve

docs-open: ## On Linux open the documentation in a browser
	xdg-open ./docs/site/index.html
