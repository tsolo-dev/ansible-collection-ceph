# tsolo.ceph/cephadm_setup_cluster

Setup a Ceph cluster using cephadm

## Table of content

- [Default Variables](#default-variables)
  - [ceph_fsid](#ceph_fsid)
  - [ceph_mds_count](#ceph_mds_count)
  - [ceph_mgr_count](#ceph_mgr_count)
  - [ceph_mon_count](#ceph_mon_count)
  - [cephadm_user](#cephadm_user)
- [Dependencies](#dependencies)
- [License](#license)
- [Author](#author)

---

## Default Variables

### ceph_fsid

The FSID of the Ceph cluster. This is the cluster identifier and should be unique.

#### Default value

```YAML
ceph_fsid: "{{ undef(hint='Ceph cluster FSID must be defined') }}"
```

### ceph_mds_count

#### Default value

```YAML
ceph_mds_count: 3
```

### ceph_mgr_count

#### Default value

```YAML
ceph_mgr_count: 3
```

### ceph_mon_count

#### Default value

```YAML
ceph_mon_count: 3
```

### cephadm_user

The Unix user name to use for Cephadm.

#### Default value

```YAML
cephadm_user: cephadm
```



## Dependencies

- ordereddict([('role', 'configuration')])

## License

Apache-2.0

## Author

Tsolo.io
