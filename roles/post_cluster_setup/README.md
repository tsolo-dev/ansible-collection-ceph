# tsolo.ceph/post_cluster_setup

Perform final steps after the cluster has been setup.

## Table of content

- [Default Variables](#default-variables)
  - [ceph_auth](#ceph_auth)
  - [ceph_extra_pools](#ceph_extra_pools)
- [Dependencies](#dependencies)
- [License](#license)
- [Author](#author)

---

## Default Variables

### ceph_auth

Create Ceph authentication tokens. These are used for access pools directly through the RADOS protocol. The variable is a list of keys to create. Each key is defined as a dictionary with the following fields:

- name (required) The name of the key. In Ceph "client." is prefixed to the name. caps (required) The a dictionary of capabilities service is the key.

#### Default value

```YAML
ceph_auth: []
```

#### Example usage

```YAML
ceph_auth:
  - name: foobar
    caps:
      mon: 'allow r'
      osd: 'allow rwx pool=foobar.p1, allow rwx pool=foobar.p2'
    key: bla2131231231
```

### ceph_extra_pools

A list of additional pools that will be created. Each item in the list can have the following fields:

- name (required) The name of the pool. - crush_rule (required) The crush rule to use for the pool. - application (required) The application the pool is used for. - config (optional) Dictionary with pool config. It is better to use the pool config variable. See pools role on how to add pool specific configuration. To add additional pools to RGW or MDS service use the extra pools option provided by those roles.

#### Default value

```YAML
ceph_extra_pools: []
```

#### Example usage

```YAML
ceph_extra_pools:
  - name: rgw-slow
    crush_rule: replicated_host_hdd
  - name: rgw-fast
    crush_rule: ec_k6_m2_host_nvme
```



## Dependencies

None.

## License

Apache-2.0

## Author

Tsolo.io
