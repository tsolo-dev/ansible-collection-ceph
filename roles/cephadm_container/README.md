# tsolo.ceph/cephadm_container

Create and deploy a container to run as a daemon where you can SSH into to run Cephadm commands.

## Table of content

- [Dependencies](#dependencies)
- [License](#license)
- [Author](#author)

---



## Dependencies

- ordereddict([('role', 'configuration')])

## License

Apache-2.0

## Author

Tsolo.io
