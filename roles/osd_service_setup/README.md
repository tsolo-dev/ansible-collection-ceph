# tsolo.ceph/osd_service_setup

## Table of content

- [Default Variables](#default-variables)
  - [ceph_osd_services](#ceph_osd_services)
- [Dependencies](#dependencies)
- [License](#license)
- [Author](#author)

---

## Default Variables

### ceph_osd_services

A list of service definitions. Each service definition will be written to a file and applied.For details on writing an OSD service definition see: https://docs.ceph.com/en/quincy/cephadm/services/osd/

#### Default value

```YAML
ceph_osd_services: []
```

#### Example usage

```YAML
ceph_osd_services:
- service_id: "osd_spec_nvme"
  placement:
    host_pattern: "*"
  spec:
    data_devices:
      model: "NVME Model Name"
    crush_device_class: "nvme"
- service_id: "osd_spec_old"
  placement:
    host_pattern: "srv*"
  spec:
    data_devices:
      model: "Disk"
- service_id: default_drive_group
  # This service example is a catch all
  # and should not be used with other.
  placement:
    host_pattern: '*'
  spec:
    data_devices:
      all: true
```



## Dependencies

None.

## License

Apache-2.0

## Author

Tsolo.io
