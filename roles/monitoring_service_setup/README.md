# tsolo.ceph/monitoring_service_setup

Configure and start the monitoring and logging services in Ceph.

## Table of content

- [Default Variables](#default-variables)
  - [ceph_dashboard_users](#ceph_dashboard_users)
  - [ceph_external_domain](#ceph_external_domain)
  - [ceph_host_node_exporters](#ceph_host_node_exporters)
  - [ceph_notification_slack](#ceph_notification_slack)
  - [ceph_notifications_slack](#ceph_notifications_slack)
- [Discovered Tags](#discovered-tags)
- [Dependencies](#dependencies)
- [License](#license)
- [Author](#author)

---

## Default Variables

### ceph_dashboard_users

A dictionary of usernames and password. The admin password cannot be set with this dictionary, use ceph_dashboard_admin_password from tsolo.ceph/configure_cluster role.

#### Default value

```YAML
ceph_dashboard_users: {}
```

#### Example usage

```YAML
ceph_dashboard_users:
  bob:
    password: secretpass
    roles:
      - read-only # Implied if no roles are listed
  tim:
    password: no1willGues$
    roles:
      - rgw-manager
```

### ceph_external_domain

#### Default value

```YAML
ceph_external_domain: cluster
```

### ceph_host_node_exporters

When true it is expected that Prometheus node exporters are running on the host and will not be managed by Ceph.

#### Default value

```YAML
ceph_host_node_exporters: false
```

### ceph_notification_slack

The Slack web hook API URL and the channel to send messages to. ceph_notifications_slack: api_url: https://hooks.slack.com/services/ABC/QW1233sdds channel: ceph-alerts

### ceph_notifications_slack

#### Default value

```YAML
ceph_notifications_slack: "{{ undef(hint='Slack connection info.') }}"
```

## Discovered Tags

**_ceph_**

**_traefik.http.routers.ceph-{{ item.0 }}.entrypoints=internal_**

**_traefik.http.routers.ceph-{{ item.0 }}.rule=HostRegexp(`ceph-{{ item.0 }}..+`)_**

**_{{ item.0 }}_**


## Dependencies

- configuration

## License

Apache-2.0

## Author

Tsolo.io
