# Based on command module (31 January 2023) from
# https://raw.githubusercontent.com/ansible/ansible/devel/lib/ansible/modules/command.py
# Copyright: (c) 2012, Michael DeHaan <michael.dehaan@gmail.com>, and others
# Copyright: (c) 2016, Toshio Kuratomi <tkuratomi@ansible.com>
# GNU General Public License v3.0+
# (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)


DOCUMENTATION = r"""
---
module: rgwadmin_cmd
short_description: Execute radosgw-admin command
description:
    - Supply the arguments to use for the radosgw-admin command
extends_documentation_fragment:
    - action_common_attributes
    - action_common_attributes.raw
attributes:
    check_mode:
        support: none
    diff_mode:
        support: none
    platform:
      support: full
      platforms: posix
    raw:
      support: full
options:
  argv:
    type: list
    elements: str
    description:
      - Passes the command as a list rather than a string.
notes:
    - notes
seealso:
- module: ansible.builtin.shell
- module: ansible.windows.command
author:
    - Tsolo.io <info@tsolo.io>
"""

EXAMPLES = r"""
- name: Use 'argv' to send a command as a list - leave 'command' empty
  ansible.builtin.rgwadmin_cmd:
    argv:
      - orch
      - host
      - set
"""

RETURN = r"""
msg:
  description: changed
  returned: always
  type: bool
  sample: True
start:
  description: The command execution start time.
  returned: always
  type: str
  sample: '2017-09-29 22:03:48.083128'
end:
  description: The command execution end time.
  returned: always
  type: str
  sample: '2017-09-29 22:03:48.084657'
delta:
  description: The command execution delta time.
  returned: always
  type: str
  sample: '0:00:00.001529'
stdout:
  description: The command standard output.
  returned: always
  type: str
  sample: 'Clustering node rabbit@slave1 with rabbit@master …'
stderr:
  description: The command standard error.
  returned: always
  type: str
  sample: 'ls cannot access foo: No such file or directory'
cmd:
  description: The command executed by the task.
  returned: always
  type: list
  sample:
  - echo
  - hello
rc:
  description: The command return code (0 means success).
  returned: always
  type: int
  sample: 0
stdout_lines:
  description: The command standard output split in lines.
  returned: always
  type: list
  sample: [u'Clustering node rabbit@slave1 with rabbit@master …']
stderr_lines:
  description: The command standard error split in lines.
  returned: always
  type: list
  sample: [u'ls cannot access foo: No such file or directory', u'ls …']
json:
  description: When json_format is set load the stdout as JSON.
  returned: when json_format==true
"""


from ansible.module_utils._text import to_text
from ansible.module_utils.basic import AnsibleModule

# from ansible.module_utils.tsolo_ceph import TsoloCephCommand
from ansible_collections.tsolo.ceph.plugins.module_utils.tsolo_ceph import (
    TsoloRgwadminCommand,
)


def main():

    module = AnsibleModule(
        argument_spec={
            "_uses_shell": {"type": "bool", "default": False},
            "argv": {"type": "list", "elements": "str"},
            "cephadm_shell": {"type": "bool", "default": True},
            "json_format": {"type": "bool", "default": False},
        },
        supports_check_mode=True,
    )
    argv = module.params["argv"]
    json_format = module.params["json_format"]

    rgwa = TsoloRgwadminCommand(module, json_format=json_format)
    try:
        rgwa.cmd(argv)
    except RuntimeError as error:
        rgwa.fail(error, changed=False)

    rgwa.exit(changed=True)


if __name__ == "__main__":
    main()
