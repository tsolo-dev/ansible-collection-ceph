# tsolo.ceph/rgw_service_setup

Setup Rados Gateway service (RGW).

## Table of content

- [Default Variables](#default-variables)
  - [ceph_rgw_config](#ceph_rgw_config)
  - [ceph_rgw_count](#ceph_rgw_count)
  - [ceph_rgw_crush_rule_data](#ceph_rgw_crush_rule_data)
  - [ceph_rgw_crush_rule_default](#ceph_rgw_crush_rule_default)
  - [ceph_rgw_crush_rule_index](#ceph_rgw_crush_rule_index)
  - [ceph_rgw_default_realm](#ceph_rgw_default_realm)
  - [ceph_rgw_default_zone](#ceph_rgw_default_zone)
  - [ceph_rgw_default_zonegroup](#ceph_rgw_default_zonegroup)
  - [ceph_rgw_extra_pools](#ceph_rgw_extra_pools)
  - [ceph_rgw_network_interface](#ceph_rgw_network_interface)
  - [ceph_rgw_port](#ceph_rgw_port)
  - [ceph_rgw_traefik_entrypoints](#ceph_rgw_traefik_entrypoints)
  - [ceph_rgw_traefik_rule](#ceph_rgw_traefik_rule)
  - [ceph_rgw_tsoloredauth_enabled](#ceph_rgw_tsoloredauth_enabled)
- [Discovered Tags](#discovered-tags)
- [Dependencies](#dependencies)
- [License](#license)
- [Author](#author)

---

## Default Variables

### ceph_rgw_config

Configuration values for the RGW service. All the key-value pairs in ceph_rgw_config will be applied with the command `ceph config set client.rgw <key> <value`. Use string for value to avoid parsing errors.

#### Default value

```YAML
ceph_rgw_config: {}
```

#### Example usage

```YAML
ceph_rgw_config:
  rgw_get_obj_max_req_size: "20971520"
  rgw_max_chunk_size: 20971520
  rgw_obj_stripe_size: 20971520
  rgw_get_obj_window_size: 20971520
  rgw_put_obj_min_window_size: 20971520
```

### ceph_rgw_count

The number of RGW instances to run. This value is passed to CephADM, CephADM will start RGW services on nodes with the **rgw** label.

#### Default value

```YAML
ceph_rgw_count: 3
```

### ceph_rgw_crush_rule_data

#### Default value

```YAML
ceph_rgw_crush_rule_data: '{{ ceph_rgw_crush_rule_default }}'
```

### ceph_rgw_crush_rule_default

Crush rule to use for the rgw data pool.

#### Default value

```YAML
ceph_rgw_crush_rule_default: replicated_rule
```

### ceph_rgw_crush_rule_index

#### Default value

```YAML
ceph_rgw_crush_rule_index: '{{ ceph_rgw_crush_rule_default }}'
```

### ceph_rgw_default_realm

The name of the realm that will be created and set as default realm.

#### Default value

```YAML
ceph_rgw_default_realm: ceph
```

### ceph_rgw_default_zone

The name of the zone that will be created and set as the default zone.

#### Default value

```YAML
ceph_rgw_default_zone: cluster-1
```

### ceph_rgw_default_zonegroup

The name of the zonegroup that will be created and set as the default zonegroup.

#### Default value

```YAML
ceph_rgw_default_zonegroup: datacenter-1
```

### ceph_rgw_extra_pools

A list of additional pools that will be created for RGW use. Each item in the list can have the following fields:

- name (required) The name of the pool. - crush_rule (required) The crush rule to use for the pool. - config (optional) Dictionary with pool config. It is better to use the pool config variable. See pools role on how to add pool specific configuration. The application for each pool is set to RGW.

#### Default value

```YAML
ceph_rgw_extra_pools: []
```

#### Example usage

```YAML
ceph_rgw_extra_pools:
  - name: rgw-slow
    crush_rule: replicated_host_hdd
  - name: rgw-fast
    crush_rule: ec_k6_m2_host_nvme
```

### ceph_rgw_network_interface

The network interface to use for RGW traffic. The address of this interface is given to Traefik.

#### Default value

```YAML
ceph_rgw_network_interface: '{{ ansible_default_ipv4.interface }}'
```

### ceph_rgw_port

The port number Ceph RGW will list on.

#### Default value

```YAML
ceph_rgw_port: 7480
```

### ceph_rgw_traefik_entrypoints

A list of the entry points on Traefik that the service will be accessible from.

#### Default value

```YAML
ceph_rgw_traefik_entrypoints:
  - web
  - websecure
```

### ceph_rgw_traefik_rule

The rule used in Traefik to filter out access to the service.

#### Default value

```YAML
ceph_rgw_traefik_rule: HostRegexp(`ceph-rgw..+`)
```

### ceph_rgw_tsoloredauth_enabled

#### Default value

```YAML
ceph_rgw_tsoloredauth_enabled: false
```

## Discovered Tags

**_{{ consul_tags }}_**


## Dependencies

None.

## License

Apache-2.0

## Author

Tsolo.io
