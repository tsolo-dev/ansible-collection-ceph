# Based on command module (31 January 2023) from
# https://raw.githubusercontent.com/ansible/ansible/devel/lib/ansible/modules/command.py
# Copyright: (c) 2012, Michael DeHaan <michael.dehaan@gmail.com>, and others
# Copyright: (c) 2016, Toshio Kuratomi <tkuratomi@ansible.com>
# GNU General Public License v3.0+
# (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)


DOCUMENTATION = r"""
---
module: ceph_cmd_config
short_description: Set or get ceph config
description:
    - Get the current value
    - When value is given set the config,
      only when value and current value differ
    - Always return the value before the set, current value.
attributes:
    check_mode:
        support: none
    diff_mode:
        support: none
    platform:
      support: full
      platforms: posix
    raw:
      support: full
options:
  who:
    type: str
    description:
      - ceph config who field
  name:
    type: str
    description:
      - ceph config name field
  value:
    type: str
    description:
      - The value the variable will be set to.
  argv:
    type: list
    elements: str
    description:
      - Passes the command as a list
  cephadm_shell:
    description:
      - Call ceph command via cephadm shell.
      - When true the called command will be `cephadm shell -- ceph config *argv`
      - When false the called command will be `ceph config *argv`
    type: bool
    default: true
notes:
    - none
seealso:
  - module: ansible.windows.command
author:
    - Tsolo.io
"""

EXAMPLES = r"""
- name: Set ceph config
  ansible.builtin.ceph_cmd_config:
    who: mgr
    name: option1/option2/enabled
    value: "true"

- name: Set ceph config
  ansible.builtin.ceph_cmd_config:
    who: mgr
    name: option1/option2/size
    value: "3"
"""

RETURN = r"""
msg:
  description: changed
  returned: always
  type: bool
  sample: True
start:
  description: The command execution start time.
  returned: always
  type: str
  sample: '2017-09-29 22:03:48.083128'
end:
  description: The command execution end time.
  returned: always
  type: str
  sample: '2017-09-29 22:03:48.084657'
delta:
  description: The command execution delta time.
  returned: always
  type: str
  sample: '0:00:00.001529'
stdout:
  description: The command standard output.
  returned: always
  type: str
  sample: 'Clustering node rabbit@slave1 with rabbit@master …'
stderr:
  description: The command standard error.
  returned: always
  type: str
  sample: 'ls cannot access foo: No such file or directory'
value:
  description: The value of the variable, if a change is requested this is the value before the change.
  returned: always
  type: str
cmd:
  description: The last command executed by the task.
  returned: always
  type: list
  sample:
  - ceph
  - orch
  - ls
rc:
  description: The command return code (0 means success).
  returned: always
  type: int
  sample: 0
stdout_lines:
  description: The command standard output split in lines.
  returned: always
  type: list
  sample: [u'Clustering node rabbit@slave1 with rabbit@master …']
stderr_lines:
  description: The command standard error split in lines.
  returned: always
  type: list
  sample: [u'ls cannot access foo: No such file or directory', u'ls …']
"""

from ansible.module_utils._text import to_text
from ansible.module_utils.basic import AnsibleModule
from ansible_collections.tsolo.ceph.plugins.module_utils.tsolo_ceph import (
    TsoloCephCommand,
)


def main():

    module = AnsibleModule(
        argument_spec={
            "_uses_shell": {"type": "bool", "default": False},
            "who": {},
            "name": {},
            "value": {"required": False},
            "cephadm_shell": {"type": "bool", "default": True},
        },
        supports_check_mode=True,
    )
    who = module.params["who"]
    name = module.params["name"]
    target_value = to_text(module.params["value"])
    current_value = None

    ceph = TsoloCephCommand(module, result=["value"], json_format=False)
    try:
        current_value = ceph.cmd(["config", "get", who, name])
    except RuntimeError as error:
        ceph.fail(error, changed=False)

    ceph.result_set("value", current_value)
    # we promised these in 'always' ( _lines get autoaded on action plugin)

    if current_value != target_value:

        try:
            current_value = ceph.cmd(["config", "set", who, name, target_value])
        except RuntimeError:
            ceph.fail("Could not set value", changed=False)
        # TODO: figure out check_mode
        # if not module.check_mode:
        #     args = [
        #         to_native(arg, errors="surrogate_or_strict", nonstring="simplerepr")
        #         for arg in args
        #     ]
        #     r["rc"], r["stdout"], r["stderr"] = module.run_command(
        #         args, use_unsafe_shell=shell, encoding=None
        #     )
        #     r["changed"] = True
        # else:
        #     # this is partial check_mode support, since we end up skipping if we get here
        #     r["rc"] = 0
        #     r["msg"] = "Command would have run if not in check mode"
        #     r["skipped"] = True
        #     # skipped=True and changed=True are mutually exclusive
        #     r["changed"] = False
    else:
        ceph.exit(msg="No change, value is already set", changed=False)
    ceph.exit(changed=True)


if __name__ == "__main__":
    main()
