# tsolo.ceph/rbd_service_setup

Create and configure pools for RBD.

## Table of content

- [Default Variables](#default-variables)
  - [artifacts_dir](#artifacts_dir)
  - [ceph_rbd_pools](#ceph_rbd_pools)
- [Dependencies](#dependencies)
- [License](#license)
- [Author](#author)

---

## Default Variables

### artifacts_dir

#### Default value

```YAML
artifacts_dir: '{{ inventory_dir }}/artifacts'
```

### ceph_rbd_pools

A list of Ceph pools for RBD use. The pools will be created, initialised, and Auth tokens created. Each pool definition is a dictionary with the following fields:

- name The name of the pool. - crush_rule The name of the crush rule to use for this pool. - kubernetes An optional boolean, when defined and true the pool is for kubernetes use.

#### Default value

```YAML
ceph_rbd_pools: []
```



## Dependencies

None.

## License

Apache-2.0

## Author

Tsolo.io
