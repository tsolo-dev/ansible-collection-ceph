# tsolo.ceph/destroy

Destroy Ceph and cleanup a node.

## Table of content

- [Default Variables](#default-variables)
  - [ceph_allow_the_use_of_destroy_cluster](#ceph_allow_the_use_of_destroy_cluster)
- [Dependencies](#dependencies)
- [License](#license)
- [Author](#author)

---

## Default Variables

### ceph_allow_the_use_of_destroy_cluster

For the destroy role to do destroy the cluster this variable must be set to "destroy_my_whole_ceph_cluster".

#### Default value

```YAML
ceph_allow_the_use_of_destroy_cluster: NO! dont destroy anything
```



## Dependencies

None.

## License

Apache-2.0

## Author

Tsolo.io
