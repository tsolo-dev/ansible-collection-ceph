# tsolo.ceph/container_service_setup

Setup a container service in Ceph.

## Table of content

- [Default Variables](#default-variables)
  - [ceph_container_service_config](#ceph_container_service_config)
  - [ceph_mds_crush_rule_default](#ceph_mds_crush_rule_default)
- [Dependencies](#dependencies)
- [License](#license)
- [Author](#author)

---

## Default Variables

### ceph_container_service_config

#### Default value

```YAML
ceph_container_service_config: {}
```

### ceph_mds_crush_rule_default

The definition of the service. Name is the name of the service and spec is the service specification expected by the command `ceph orch apply`. container_service_config: ttyd: spec: placement: label: "mon" count: 3 spec: image: andi91/ttyd:latest uid: 1000 gid: 1000 args: - "--publish=7681:7681" envs: - SECRET=mypassword - PORT=8000 - PUID=1000 - PGID=1000 networks: - 192.168.100.0/24



## Dependencies

None.

## License

Apache-2.0

## Author

Tsolo.io
