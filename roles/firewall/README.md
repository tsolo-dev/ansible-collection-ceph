# tsolo.ceph/firewall

## Table of content

- [Default Variables](#default-variables)
  - [ceph_firewall](#ceph_firewall)
  - [firewalld_enabled](#firewalld_enabled)
- [Dependencies](#dependencies)
- [License](#license)
- [Author](#author)

---

## Default Variables

### ceph_firewall

#### Default value

```YAML
ceph_firewall: {}
```

### firewalld_enabled

When true the firewall is configured for this node.

#### Default value

```YAML
firewalld_enabled: true
```



## Dependencies

None.

## License

Apache-2.0

## Author

Tsolo.io
