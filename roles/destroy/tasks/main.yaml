# roles/destroy/tasks/main.yaml
---
- name: Warn user that this is dangerous
  ansible.builtin.debug:
    msg:
      - "This is dangerous:"
      - "- all Ceph processes and containers will be stopped"
      - "- all Ceph mounts and LVM definitions will be removed"
      - You will lose data!!
  run_once: true
  delegate_to: localhost
  become: false

- name: Check that ceph_allow_the_use_of_destroy_cluster is set
  ansible.builtin.fail:
    msg: The variable ceph_allow_the_use_of_destroy_cluster does not have the correct value.
  when: ceph_allow_the_use_of_destroy_cluster != "destroy_my_whole_ceph_cluster"

- name: Create /opt/ceph
  ansible.builtin.file:
    path: "{{ item }}"
    state: directory
    mode: "0755"
  loop:
    - /opt/ceph

- name: Template cleanup utils
  ansible.builtin.template:
    src: "{{ item.src }}"
    dest: "{{ item.dest }}"
    mode: a+rx
  loop:
    - src: zap-local-drive.sh.j2
      dest: /opt/ceph/zap-local-drive.sh

- name: Check user confirmation
  become: false
  ansible.builtin.command: /bin/true
  failed_when: really_destroy_ceph != 'YES'
  changed_when: really_destroy_ceph != 'YES'
  any_errors_fatal: true
  delegate_to: localhost

# Stop stray services
- name: Get Ceph service
  ansible.builtin.shell: systemctl list-units --plain --no-legend | awk '/ceph/ {print $1}'
  register: ceph_services
  changed_when: false

- name: Stop and disable services
  ansible.builtin.systemd:
    name: "{{ item }}"
    state: stopped
    enabled: false
  loop: "{{ ceph_services.stdout_lines }}"
  failed_when: false
  # ignore_errors: true # Ceph-ansible leaves broken services

# Unmount stray volumes
- name: Get Ceph mounts
  ansible.builtin.shell: mount | awk ' /ceph.osd/ {print $3}'
  register: ceph_mounts
  # TODO: Ansible facts already has mounts. rather use ansible_facts.mounts
  # .e.g. msg: "{{(ansible_facts.mounts | selectattr('mount', 'in', path)
  # | list | sort(attribute='mount'))[-1]['mount']}}"
  changed_when: false

- name: Unmount Ceph
  ansible.posix.mount:
    path: "{{ item }}"
    state: unmounted
  loop: "{{ ceph_mounts.stdout_lines }}"

# Delete files and directories
- name: Find stray systemd files
  ansible.builtin.find:
    path: /etc/systemd/system
    pattern: ceph*,*ceph*
  register: systemd_spec

- name: Delete systemd files
  ansible.builtin.file:
    path: "{{ item.path }}"
    state: absent
  loop: "{{ systemd_spec.files }}"

- name: Reload systemd
  ansible.builtin.systemd:
    daemon_reload: true
  when: systemd_spec.files | length > 0

- name: Delete Ceph directories and files
  ansible.builtin.file:
    path: "{{ item }}"
    state: absent
  loop:
    - /var/lib/ceph
    - /var/run/ceph
    - /var/log/ceph
    - /etc/ceph
    - /etc/apt/sources.list.d/ceph.list
    - /etc/profile.d/cephadm.sh
    - /etc/logrotate.d/ceph
    - /opt/cephadm # Old version of this collection used to install scripts into /opt/cephadm not /opt/ceph

# Uninstall Ceph
- name: Get Ceph packages
  ansible.builtin.shell: dpkg -l | awk ' /ceph/ {print $2}'
  register: ceph_packages
  changed_when: false

- name: Remove package
  ansible.builtin.apt:
    name: "{{ item }}"
    state: absent
    purge: true
  loop: "{{ ceph_packages.stdout_lines }}"
  notify: cleanup apt

# Remove containers
- name: Get Podman binary
  ansible.builtin.command: which podman
  register: which_podman
  changed_when: false
  ignore_errors: true

- name: Get Docker binary
  ansible.builtin.command: which docker
  register: which_docker
  changed_when: false
  ignore_errors: true

- name: Extract Docker/Podman condition
  ansible.builtin.set_fact:
    docker_bin: "{{ which_docker.stdout }}"
    podman_bin: "{{ which_podman.stdout }}"

- name: Report the location of docker and podman binary files
  ansible.builtin.debug:
    msg: Docker={{ docker_bin }} Podman={{ podman_bin }}

- name: List running Ceph Docker containers
  ansible.builtin.shell: "{{ docker_bin }} ps | awk '/ceph/ {print $1}'"
  register: ceph_docker
  changed_when: false
  when: docker_bin != ''

- name: Remove running Ceph Docker containers
  ansible.builtin.command: "{{ docker_bin }} stop {{ item }}"
  loop: "{{ ceph_docker.stdout_lines }}"
  when: docker_bin != '' and ceph_docker.stdout_lines

- name: List running Ceph Podman containers
  ansible.builtin.shell: "{{ podman_bin }} ps | awk '/ceph/ {print $1}'"
  register: ceph_podman
  changed_when: false
  when: podman_bin != ''

- name: Remove running Ceph Podman containers
  ansible.builtin.command: "{{ podman_bin }} stop {{ item }}"
  loop: "{{ ceph_podman.stdout_lines }}"
  when: podman_bin != '' and ceph_podman.stdout_lines

# Remove volumes used for Ceph
- name: List Logical Volumes (LV) use by Ceph
  ansible.builtin.shell: lvdisplay | grep "LV Path" | awk '/ceph/ {print $3}'
  register: ceph_lv
  changed_when: false

- name: Remove Ceph LV
  ansible.builtin.command: lvremove -y {{ item }}
  loop: "{{ ceph_lv.stdout_lines }}"
  when: ceph_lv.stdout_lines

- name: List Volume Group (VG) use by Ceph
  ansible.builtin.shell: vgs | awk '/ceph/ {print $1}'
  register: ceph_vg
  changed_when: false

- name: List Physical Volumes (PV) use by Ceph
  ansible.builtin.shell: pvs | awk '/ceph/ {print $1}'
  register: ceph_pv
  changed_when: false

- name: Remove Ceph VG
  ansible.builtin.command: vgremove -y {{ item }}
  loop: "{{ ceph_vg.stdout_lines }}"
  when: ceph_vg.stdout_lines

- name: Remove Ceph PV
  ansible.builtin.command: pvremove -y {{ item }}
  loop: "{{ ceph_pv.stdout_lines }}"
  when: ceph_pv.stdout_lines

- name: Remove Ceph PV
  ansible.builtin.command: /opt/ceph/zap-local-drive.sh {{ item }}
  loop: "{{ ceph_pv.stdout_lines }}"
  when: ceph_pv.stdout_lines

# TODO:
# Remove ceph file in /etc/logrotate.d
