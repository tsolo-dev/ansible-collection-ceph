# Configuration Options


## tsolo.ceph/cephadm_install

Install Cephadm

### artifacts_dir



#### Default value:

```yaml
artifacts_dir: '{{ inventory_dir }}/artifacts'
```

### ceph_version

The Ceph version to install.

#### Default value:

```yaml
ceph_version: 17.2.5
```

### cephadm_ceph_image

The Ceph image to use for Cephadm.

#### Default value:

```yaml
cephadm_ceph_image: quay.io/ceph/ceph:v{{ ceph_version }}
```

### cephadm_user

The Unix user name to use for Cephadm.

#### Default value:

```yaml
cephadm_user: cephadm
```

## tsolo.ceph/cephadm_setup_cluster

Setup a Ceph cluster using cephadm

### ceph_fsid

The FSID of the Ceph cluster. This is the cluster identifier and should be unique.

#### Default value:

```yaml
ceph_fsid: "{{ undef(hint='Ceph cluster FSID must be defined') }}"
```

### ceph_mds_count



#### Default value:

```yaml
ceph_mds_count: 3
```

### ceph_mgr_count



#### Default value:

```yaml
ceph_mgr_count: 3
```

### ceph_mon_count



#### Default value:

```yaml
ceph_mon_count: 3
```

### cephadm_user

The Unix user name to use for Cephadm.

#### Default value:

```yaml
cephadm_user: cephadm
```

## tsolo.ceph/cluster_snapshot

Collect information that can be used to create a snapshot view of the cluster.

### ceph_cluster_snapshot_health_only



#### Default value:

```yaml
ceph_cluster_snapshot_health_only: false
```

### ceph_cluster_snapshot_location

The base directory where the cluster_snapshots are stored.
Each snapshot event is stored in a timestamped directory.

#### Default value:

```yaml
ceph_cluster_snapshot_location: /var/lib/ceph/cluster_snapshot
```

### ceph_cluster_snapshot_timestamp



#### Default value:

```yaml
ceph_cluster_snapshot_timestamp: '{{ ansible_date_time.iso8601 }}'
```

## configuration



### ceph_version



#### Default value:

```yaml
ceph_version: 17.2.5
```

### cephadm_user



#### Default value:

```yaml
cephadm_user: cephadm
```

### firewalld_enabled



#### Default value:

```yaml
firewalld_enabled: false
```

## tsolo.ceph/configure_cluster

Configure a Ceph cluster

### ceph_crush_rules

A list of crush rules to create.
Each crush rule can have the following fields.


- failure_domain - default to 'host'.
- root - default to 'default'.
- class - the device class, default to 'all'.
- ec_k - K value for erasure coded pool. If this exists it crush rule is erasure and not replicated, default to 0.
- ec_m - M value for erasure coded pool. default to 2.

#### Example

```yaml
ceph_crush_rules:
  - failure_domain: osd
  - class: hdd
  - class: ssd
  - class: nvme
  - ec_k: 2
    ec_m: 2
```

#### Default value:

```yaml
ceph_crush_rules: []
```

### ceph_dashboard_admin_password

The initial password for the dashboard admin user.

#### Default value:

```yaml
ceph_dashboard_admin_password: Password*12345
```
This is a secret, the value of production and development is not shown.

### ceph_mgr_config

Configuration values at the base of the configuration hierarchy.
It is best to use the service specific config options like ceph_rgw_config.
All the key-value pairs in
ceph_mgr_config will be applied with the command
`ceph config set global <key> <value>`.
Use string for value to avoid parsing errors.

#### Example

```yaml
ceph_mgr_config:
  mgr_val: 9.2
```

#### Default value:

```yaml
ceph_mgr_config: {}
```

### ceph_mon_config

Configuration values at the base of the configuration hierarchy.
It is best to use the service specific config options like ceph_rgw_config.
All the key-value pairs in
ceph_mon_config will be applied with the command
`ceph config set global <key> <value>`.
Use string for value to avoid parsing errors.

#### Example

```yaml
ceph_mon_config:
  mon_val: 200
```

#### Default value:

```yaml
ceph_mon_config: {}
```

### ceph_osd_config

Configuration values at the base of the configuration hierarchy.
It is best to use the service specific config options like ceph_rgw_config.
All the key-value pairs in
ceph_osd_config will be applied with the command
`ceph config set global <key> <value>`.
Use string for value to avoid parsing errors.

#### Example

```yaml
ceph_osd_config:
  osd_val: "true"
```

#### Default value:

```yaml
ceph_osd_config: {}
```

### ceph_telemetry_enabled

When enabled telemetry information about the cluster will be sent to Ceph foundation.

#### Default value:

```yaml
ceph_telemetry_enabled: true
```

## tsolo.ceph/container_service_setup

Setup a container service in Ceph.

### ceph_container_service_config



#### Default value:

```yaml
ceph_container_service_config: {}
```

### ceph_mds_crush_rule_default

The definition of the service. Name is the name of the service and spec is the
service specification expected by the command `ceph orch apply`.
container_service_config:
  ttyd:
    spec:
      placement:
        label: "mon"
        count: 3
      spec:
        image: andi91/ttyd:latest
        uid: 1000
        gid: 1000
        args:
          - "--publish=7681:7681"
        envs:
          - SECRET=mypassword
          - PORT=8000
          - PUID=1000
          - PGID=1000
      networks:
      - 192.168.100.0/24

## tsolo.ceph/destroy

Destroy Ceph and cleanup a node.

### ceph_allow_the_use_of_destroy_cluster

For the destroy role to do destroy the cluster this variable must be set to
"destroy_my_whole_ceph_cluster".

#### Default value:

```yaml
ceph_allow_the_use_of_destroy_cluster: NO! dont destroy anything
```

## tsolo.ceph/firewall



### ceph_firewall



#### Default value:

```yaml
ceph_firewall: {}
```

### firewalld_enabled

When true the firewall is configured for this node.

#### Default value:

```yaml
firewalld_enabled: true
```

## tsolo.ceph/mds_service_setup

Setup MDS and create CephFS share.

### ceph_mds_crush_rule_data



#### Default value:

```yaml
ceph_mds_crush_rule_data: '{{ ceph_mds_crush_rule_default }}'
```

### ceph_mds_crush_rule_default

Crush rule to use for the mds data pool. EC pools are allowed but discouraged.

#### Default value:

```yaml
ceph_mds_crush_rule_default: replicated_rule
```

### ceph_mds_crush_rule_meta



#### Default value:

```yaml
ceph_mds_crush_rule_meta: '{{ ceph_mds_crush_rule_default }}'
```

## tsolo.ceph/monitoring_service_setup

Configure and start the monitoring and logging services in Ceph.

### ceph_dashboard_users

A dictionary of usernames and password.
The admin password cannot be set with this dictionary, use
ceph_dashboard_admin_password from tsolo.ceph/configure_cluster role.

#### Example

```yaml
ceph_dashboard_users:
  bob:
    password: secretpass
    roles:
      - read-only # Implied if no roles are listed
  tim:
    password: no1willGues$
    roles:
      - rgw-manager
```

#### Default value:

```yaml
ceph_dashboard_users: {}
```

### ceph_external_domain



#### Default value:

```yaml
ceph_external_domain: cluster
```

### ceph_host_node_exporters

When true it is expected that Prometheus node exporters are running on the host and will not be managed by Ceph.

#### Default value:

```yaml
ceph_host_node_exporters: false
```

### ceph_notification_slack

The Slack web hook API URL and the channel to send messages to.
ceph_notifications_slack:
  api_url: https://hooks.slack.com/services/ABC/QW1233sdds
  channel: ceph-alerts

### ceph_notifications_slack



#### Default value:

```yaml
ceph_notifications_slack: "{{ undef(hint='Slack connection info.') }}"
```

## tsolo.ceph/osd_service_setup



### ceph_osd_services

A list of service definitions. Each service definition will be written to a
file and applied.For details on writing an OSD service definition see:
https://docs.ceph.com/en/quincy/cephadm/services/osd/

#### Example

```yaml
ceph_osd_services:
- service_id: "osd_spec_nvme"
  placement:
    host_pattern: "*"
  spec:
    data_devices:
      model: "NVME Model Name"
    crush_device_class: "nvme"
- service_id: "osd_spec_old"
  placement:
    host_pattern: "srv*"
  spec:
    data_devices:
      model: "Disk"
- service_id: default_drive_group
  # This service example is a catch all
  # and should not be used with other.
  placement:
    host_pattern: '*'
  spec:
    data_devices:
      all: true
```

#### Default value:

```yaml
ceph_osd_services: []
```

## tsolo.ceph/pools

Create and configure storage pools in Ceph.

### ceph_pool_name_of_pool_config

Set per pool configuration values. A variable is defined for each pool.
The name of the variable starts with "ceph_pool_", then a safe form of the pool
name followed by "_config". To create a safe form of the pool name replace all
'.' and '-' characters in the pool name with '_', i.e. slugify the pool name.
Example, for the pool 'cluster-1.rgw.buckets.non-ec' the config will look for
variable named 'ceph_pool_cluster_1_rgw_buckets_non_ec_config'.
The pool config variable is a dictionary. The role takes all key-value
pairs and use the command ceph `osd pool set <pool-name> <key> <value>` to
apply the config. Thus any config key available to `osd pool set` is allowed.


Supports all the options available to `osd pool set <pool> <var> <value>`.


Where `var` is one of *size, min_size, pg_num, pgp_num, pgp_num_actual, crush_rule, hashpspool,
nodelete, nopgchange, nosizechange ,  write_fadvise_dontneed, noscrub,
nodeep-scrub, hit_set_type, hit_set_period, hit_set_count, hit_set_fpp,
use_gmt_hitset, target_max_bytes, target_max_objects,
cache_target_dirty_ratio, cache_target_dirty_high_ratio,
cache_target_full_ratio, cache_min_flush_age, cache_min_evict_age,
min_read_recency_for_promote ,  min_write_recency_for_promote, fast_read,
hit_set_grade_decay_rate, hit_set_search_last_n, scrub_min_interval,
scrub_max_interval, deep_scrub_interval, recovery_priority,
recovery_op_priority, scrub_priority, compression_mode,
compression_algorithm, compression_required_ratio, compression_max_blob_size,
compression_min_blob_size, csum_type, csum_min_block, csum_max_block,
allow_ec_overwrites, fingerprint_algorithm, pg_autoscale_mode,
pg_autoscale_bias, pg_num_min, pg_num_max, target_size_bytes, target_size_ratio,
dedup_tier, dedup_chunk_algorithm, dedup_cdc_chunk_size, eio, bulk*.


All values are first passed to string. It is advised to use strings for all
values in your config.
Values conversion is not always compatible with Ceph C++
type casting e.g. boolean cause problems since Python creates "True/False"
but Ceph expects "true/false".

#### Example

```yaml
ceph_pool_name_of_pool_config:
  bulk: "true"
  pg_num: 512
  pgp_num: 512
  pg_autoscale_mode: "warn"
```

#### Default value:

```yaml
ceph_pool_name_of_pool_config: This variable is not used, it is here for documentation purposes.
```

## tsolo.ceph/post_cluster_setup

Perform final steps after the cluster has been setup.

### ceph_auth

Create Ceph authentication tokens. These are used for access pools
directly through the RADOS protocol.
The variable is a list of keys to create. Each key is defined as a dictionary
with the following fields:


- name (required) The name of the key. In Ceph "client." is prefixed to the name.
  caps (required) The a dictionary of capabilities service is the key.

#### Example

```yaml
ceph_auth:
  - name: foobar
    caps:
      mon: 'allow r'
      osd: 'allow rwx pool=foobar.p1, allow rwx pool=foobar.p2'
    key: bla2131231231
```

#### Default value:

```yaml
ceph_auth: []
```

### ceph_extra_pools

A list of additional pools that will be created.
Each item in the list can have the following fields:


- name (required) The name of the pool.
- crush_rule (required) The crush rule to use for the pool.
- application (required) The application the pool is used for.
- config (optional) Dictionary with pool config. It is better to use the pool
  config variable. See pools role on how to add pool specific configuration.
To add additional pools to RGW or MDS service use the extra pools option
provided by those roles.

#### Example

```yaml
ceph_extra_pools:
  - name: rgw-slow
    crush_rule: replicated_host_hdd
  - name: rgw-fast
    crush_rule: ec_k6_m2_host_nvme
```

#### Default value:

```yaml
ceph_extra_pools: []
```

## tsolo.ceph/rbd_service_setup

Create and configure pools for RBD.

### artifacts_dir



#### Default value:

```yaml
artifacts_dir: '{{ inventory_dir }}/artifacts'
```

### ceph_rbd_pools

A list of Ceph pools for RBD use.
The pools will be created, initialised, and Auth tokens created.
Each pool definition is a dictionary with the following fields:


- name The name of the pool.
- crush_rule The name of the crush rule to use for this pool.
- kubernetes An optional boolean, when defined and true the pool is for kubernetes use.

#### Default value:

```yaml
ceph_rbd_pools: []
```

## tsolo.ceph/rgw_service_setup

Setup Rados Gateway service (RGW).

### ceph_rgw_config

Configuration values for the RGW service. All the key-value pairs in
ceph_rgw_config will be applied with the command
`ceph config set client.rgw <key> <value`.
Use string for value to avoid parsing errors.

#### Example

```yaml
ceph_rgw_config:
  rgw_get_obj_max_req_size: "20971520"
  rgw_max_chunk_size: 20971520
  rgw_obj_stripe_size: 20971520
  rgw_get_obj_window_size: 20971520
  rgw_put_obj_min_window_size: 20971520
```

#### Default value:

```yaml
ceph_rgw_config: {}
```

### ceph_rgw_count

The number of RGW instances to run. This value is passed to CephADM, CephADM
will start RGW services on nodes with the **rgw** label.

#### Default value:

```yaml
ceph_rgw_count: 3
```

### ceph_rgw_crush_rule_data



#### Default value:

```yaml
ceph_rgw_crush_rule_data: '{{ ceph_rgw_crush_rule_default }}'
```

### ceph_rgw_crush_rule_default

Crush rule to use for the rgw data pool.

#### Default value:

```yaml
ceph_rgw_crush_rule_default: replicated_rule
```

### ceph_rgw_crush_rule_index



#### Default value:

```yaml
ceph_rgw_crush_rule_index: '{{ ceph_rgw_crush_rule_default }}'
```

### ceph_rgw_default_realm

The name of the realm that will be created and set as default realm.

#### Default value:

```yaml
ceph_rgw_default_realm: ceph
```

### ceph_rgw_default_zone

The name of the zone that will be created and set as the default zone.

#### Default value:

```yaml
ceph_rgw_default_zone: cluster-1
```

### ceph_rgw_default_zonegroup

The name of the zonegroup that will be created and set as the default zonegroup.

#### Default value:

```yaml
ceph_rgw_default_zonegroup: datacenter-1
```

### ceph_rgw_extra_pools

A list of additional pools that will be created for RGW use.
Each item in the list can have the following fields:


- name (required) The name of the pool.
- crush_rule (required) The crush rule to use for the pool.
- config (optional) Dictionary with pool config. It is better to use the pool
  config variable. See pools role on how to add pool specific configuration.
The application for each pool is set to RGW.

#### Example

```yaml
ceph_rgw_extra_pools:
  - name: rgw-slow
    crush_rule: replicated_host_hdd
  - name: rgw-fast
    crush_rule: ec_k6_m2_host_nvme
```

#### Default value:

```yaml
ceph_rgw_extra_pools: []
```

### ceph_rgw_network_interface

The network interface to use for RGW traffic. The address of this interface is given to Traefik.

#### Default value:

```yaml
ceph_rgw_network_interface: '{{ ansible_default_ipv4.interface }}'
```

### ceph_rgw_port

The port number Ceph RGW will list on.

#### Default value:

```yaml
ceph_rgw_port: 7480
```

### ceph_rgw_traefik_entrypoints

A list of the entry points on Traefik that the service will be accessible from.

#### Default value:

```yaml
ceph_rgw_traefik_entrypoints:
- web
- websecure
```

### ceph_rgw_traefik_rule

The rule used in Traefik to filter out access to the service.

#### Default value:

```yaml
ceph_rgw_traefik_rule: HostRegexp(`ceph-rgw..+`)
```

### ceph_rgw_tsoloredauth_enabled



#### Default value:

```yaml
ceph_rgw_tsoloredauth_enabled: false
```