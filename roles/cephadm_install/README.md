# tsolo.ceph/cephadm_install

Install Cephadm

## Table of content

- [Default Variables](#default-variables)
  - [artifacts_dir](#artifacts_dir)
  - [ceph_version](#ceph_version)
  - [cephadm_ceph_image](#cephadm_ceph_image)
  - [cephadm_user](#cephadm_user)
- [Dependencies](#dependencies)
- [License](#license)
- [Author](#author)

---

## Default Variables

### artifacts_dir

#### Default value

```YAML
artifacts_dir: '{{ inventory_dir }}/artifacts'
```

### ceph_version

The Ceph version to install.

#### Default value

```YAML
ceph_version: 17.2.5
```

### cephadm_ceph_image

The Ceph image to use for Cephadm.

#### Default value

```YAML
cephadm_ceph_image: quay.io/ceph/ceph:v{{ ceph_version }}
```

### cephadm_user

The Unix user name to use for Cephadm.

#### Default value

```YAML
cephadm_user: cephadm
```



## Dependencies

- ordereddict([('role', 'configuration')])

## License

Apache-2.0

## Author

Tsolo.io
