# SPDX-FileCopyrightText: 2023-present Martin Slabber <martin@tsolo.io>
#
# SPDX-License-Identifier: MIT
__version__ = "0.0.1"
