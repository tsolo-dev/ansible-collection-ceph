# tsolo.ceph/mds_service_setup

Setup MDS and create CephFS share.

## Table of content

- [Default Variables](#default-variables)
  - [ceph_mds_crush_rule_data](#ceph_mds_crush_rule_data)
  - [ceph_mds_crush_rule_default](#ceph_mds_crush_rule_default)
  - [ceph_mds_crush_rule_meta](#ceph_mds_crush_rule_meta)
- [Dependencies](#dependencies)
- [License](#license)
- [Author](#author)

---

## Default Variables

### ceph_mds_crush_rule_data

#### Default value

```YAML
ceph_mds_crush_rule_data: '{{ ceph_mds_crush_rule_default }}'
```

### ceph_mds_crush_rule_default

Crush rule to use for the mds data pool. EC pools are allowed but discouraged.

#### Default value

```YAML
ceph_mds_crush_rule_default: replicated_rule
```

### ceph_mds_crush_rule_meta

#### Default value

```YAML
ceph_mds_crush_rule_meta: '{{ ceph_mds_crush_rule_default }}'
```



## Dependencies

None.

## License

Apache-2.0

## Author

Tsolo.io
