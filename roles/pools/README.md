# tsolo.ceph/pools

Create and configure storage pools in Ceph.

## Table of content

- [Default Variables](#default-variables)
  - [ceph_pool_name_of_pool_config](#ceph_pool_name_of_pool_config)
- [Dependencies](#dependencies)
- [License](#license)
- [Author](#author)

---

## Default Variables

### ceph_pool_name_of_pool_config

Set per pool configuration values. A variable is defined for each pool. The name of the variable starts with "ceph_pool_", then a safe form of the pool name followed by "_config". To create a safe form of the pool name replace all '.' and '-' characters in the pool name with '_', i.e. slugify the pool name. Example, for the pool 'cluster-1.rgw.buckets.non-ec' the config will look for variable named 'ceph_pool_cluster_1_rgw_buckets_non_ec_config'. The pool config variable is a dictionary. The role takes all key-value pairs and use the command ceph `osd pool set <pool-name> <key> <value>` to apply the config. Thus any config key available to `osd pool set` is allowed.

Supports all the options available to `osd pool set <pool> <var> <value>`.

Where `var` is one of *size, min_size, pg_num, pgp_num, pgp_num_actual, crush_rule, hashpspool, nodelete, nopgchange, nosizechange , write_fadvise_dontneed, noscrub, nodeep-scrub, hit_set_type, hit_set_period, hit_set_count, hit_set_fpp, use_gmt_hitset, target_max_bytes, target_max_objects, cache_target_dirty_ratio, cache_target_dirty_high_ratio, cache_target_full_ratio, cache_min_flush_age, cache_min_evict_age, min_read_recency_for_promote , min_write_recency_for_promote, fast_read, hit_set_grade_decay_rate, hit_set_search_last_n, scrub_min_interval, scrub_max_interval, deep_scrub_interval, recovery_priority, recovery_op_priority, scrub_priority, compression_mode, compression_algorithm, compression_required_ratio, compression_max_blob_size, compression_min_blob_size, csum_type, csum_min_block, csum_max_block, allow_ec_overwrites, fingerprint_algorithm, pg_autoscale_mode, pg_autoscale_bias, pg_num_min, pg_num_max, target_size_bytes, target_size_ratio, dedup_tier, dedup_chunk_algorithm, dedup_cdc_chunk_size, eio, bulk*.

All values are first passed to string. It is advised to use strings for all values in your config. Values conversion is not always compatible with Ceph C++ type casting e.g. boolean cause problems since Python creates "True/False" but Ceph expects "true/false".

#### Default value

```YAML
ceph_pool_name_of_pool_config: This variable is not used, it is here for documentation
  purposes.
```

#### Example usage

```YAML
ceph_pool_name_of_pool_config:
  bulk: "true"
  pg_num: 512
  pgp_num: 512
  pg_autoscale_mode: "warn"
```



## Dependencies

None.

## License

Apache-2.0

## Author

Tsolo.io
